## Implementierung

Im package `de.hhu.dbs.shop.tables` finden Sie Klassen, welche alle benötigten Tabellen darstellen. Jede dieser Klassen gibt grundlegende Methoden, die Sie implementieren müssen, vor. Als Beispiel wird hier die (unvollstädige) Methode zum Abrufen eines bestimmten Artikels anhand seiner eindeutigen Id gezeigt.

```java
    // de.hhu.dbs.shop.tables.ArticlesImpl

    /**
     * Fragt einen Artikel anhand seiner eindeutigen Id ab.
     *
     * @param id Die Id des zu suchenden Artikels
     * @return Den gefundenen Artikel oder {@code null} falls keiner gefunden wurde
     */
    @Override
    @Nullable
    public Article getById(long id) {
        return null;
    }
```

Ihre Aufgabe ist es nun den Methodenkörper so abzuändern, dass die Funktion die im Kommentar enthaltene Anforderung erfüllt. Achten Sie hierbei insbesondere auf eine korrekte Überführung Ihrer Tabelleneinträge in die entsprechenden Objekte. Jedes Objekt besitzt genau einen Konstruktor, welcher alle nötigen Parameter vorgibt.

Innerhalb aller Klassen des packages `de.hhu.dbs.shop.tables` steht Ihnen hierfür die Variablen `database` (`de.hhu.dbs.warehouse.db.Database`) sowie `connection` (`java.sql.Connection`) zur Verfügung. Mit Hilfe der `Connection` können Sie selbst formulierte SQLite-Anfragen an die verbundene Datenbank stellen. Die `Database`-Instanz gibt Ihnen die Möglichkeit auf bereits implementierte Methoden anderer Tabellen zuzugreifen. Sollten Sie beispielsweise die oben gezeigte Methode bereits implementiert haben, können Sie wie folgt auf diese  zugreifen.

```java
database.getArticles().getById(42);
```

Für eine kurze Übersicht zur Nutzung des SQLite Treibers unter Java können Sie Sich die folgenden Tutorials anschauen.

  * [SQLite Java: Inserting Data](http://www.sqlitetutorial.net/sqlite-java/insert/)
  * [SQLite Java: Select Data](http://www.sqlitetutorial.net/sqlite-java/select/)
  * [SQLite Java: Update Data](http://www.sqlitetutorial.net/sqlite-java/update/)
  * [SQLite Java: Deleting Data](http://www.sqlitetutorial.net/sqlite-java/delete/)
  * [SQLite Java: Transaction](http://www.sqlitetutorial.net/sqlite-java/transaction/)
  * [SQLite Java: Write and Read BLOB](http://www.sqlitetutorial.net/sqlite-java/jdbc-read-write-blob/)

Alternativ steht das durch Oracle bereitgestellte [JDBC Tutorial](https://docs.oracle.com/javase/tutorial/jdbc/index.html) zur Verfügung.

Nachdem Sie die grundlegenden Tabellen-Methoden implementiert haben, müssen schließlich die vorgegebenen Methoden der Aufgabenstellung umgesetzt werden. Hierfür wird Ihnen die Klasse `Shop` (`de.hhu.dbs.shop.Shop`) bereitgestellt. Diese enthält alle Methoden, die für die Erfüllung der Aufgabenstellung nötig sind. Als Beispiel wird hier die (unvollständige) Methode zur Erstellung eines Newsletters gezeigt.

```java
    /**
     * Erstellt einen neuen Newsletter, falls möglich.
     * 
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param newsletter Der zu erstellende Newsletter
     */
    @Override
    public void createNewsletter(Newsletter newsletter) {

    }
```

Achten Sie bei der Implementierung insbesondere auf die in den Kommentaren enthaltenen Hinweise. Den aktuell eingeloggten Kunden können Sie über die Variable `currentUser` erfragen. Falls kein eingeloggter Kunde existiert, ist diese `null`. Da viele Methoden erst nach einem Login zur Verfügung stehen sollen, bietet es sich an zuerst die Methoden `register(Customer newCustomer)` und `login(String email, String password)` zu implementieren.

Innerhalb der `Shop`-Klasse können Sie ebenfalls auf die Variable `database` zugreifen und somit die zuvor implementierten Grundfunktionen nutzen. Sollten Sie dennoch Anfragen formulieren wollen, steht Ihnen die Datenbankverbindung über `database.getConnection()` zur Verfügung.

Ihre Implementierung können Sie **optional** mit Hilfe von eigenen Tests überprüfen. Im Source Set `test` finden Sie hierzu die Klasse `ShopTest` (`de.hhu.dbs.shop.ShopTest`), welche implementiert werden kann. Innerhalb dieser steht Ihnen eine `Shop`- sowie eine `Database`-Instanz zur Verfügung.

## Achtung
**Da ihre Abgabe automatisch getestet wird, dürfen Sie die bereitgestellten Klassen weder umbenennen, verschieben noch die in ihnen deklarierten Vererbungen modifizieren!**

**Eine Verletzung dieser Regelung führt zum Nichtbestehen dieses Aufgabenteils!**

