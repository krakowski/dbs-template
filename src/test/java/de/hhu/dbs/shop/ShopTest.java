package de.hhu.dbs.shop;

import de.hhu.dbs.shop.tables.*;
import de.hhu.dbs.warehouse.db.Database;
import de.hhu.dbs.warehouse.db.DatabaseInterface;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.fail;

public class ShopTest {

    private static Shop shop;

    private static Database database;

    @BeforeClass
    public static void setup() {

        DatabaseInterface dependencies = new DatabaseInterface(
                new ArticlesImpl(),
                new TagsImpl(),
                new CouriersImpl(),
                new CustomersImpl(),
                new CartsImpl(),
                new EmployeesImpl(),
                new NewslettersImpl(),
                new OffersImpl(),
                new PremiumCustomersImpl(),
                new SubscriptionsImpl(),
                new VendorsImpl()
        );

        shop = new Shop(dependencies);

        database = shop.getDatabase();
    }

    @Test
    public void register() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void login() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void updateProfile() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void subscribe() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void subscribe1() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void unsubscribe() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void makePremium() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void createCart() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void removeCart() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void getCarts() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void putOffer() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void removeOffer() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void orderCart() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void createTag() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void deleteTag() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void createArticle() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void deleteArticle() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void createOffer() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void deleteOffer() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void updateArticle() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void createNewsletter() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void getOrderedCarts() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void addRecommendation() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void removeRecommendation() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void searchByName() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void getSortedOffers() throws Exception {
        fail("Not Implemented");
    }

    @Test
    public void getArticlesByTag() throws Exception {
        fail("Not Implemented");
    }

}