package de.hhu.dbs.shop;

import de.hhu.dbs.warehouse.Warehouse;
import de.hhu.dbs.warehouse.db.DatabaseInterface;
import de.hhu.dbs.warehouse.pojo.*;

import javax.annotation.Nullable;
import javax.swing.*;
import java.time.LocalDateTime;
import java.util.List;

public class Shop extends Warehouse {

    /**
     * Tragen Sie hier Ihre Connection Url ein.
     *
     * <p>
     * <b>Hinweis</b>: Diese Url wird an {@link java.sql.DriverManager#getConnection(String)} weitergereicht.
     * </p>
     */
    private static final String CONNECTION_URL = "";

    public Shop(DatabaseInterface databaseInterface) {
        super(databaseInterface);
    }

    /**
     * Registriert einen neuen Kunden im System, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Eine Registrierung ist nicht immer möglich.
     * </p>
     *
     * @param customer Der zu registrierende Kunde.
     */
    @Override
    public void register(Customer customer) {

    }

    /**
     * Loggt einen bestehenden Kunden anhand seiner E-Mail Adresse und seinem Passwort ein.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode kann zu einem beliebigen Zeitpunkt erneut aufgerufen werden. Ein erneutes Einloggen
     * mit anderen Benutzerdaten ist also zu jeder Zeit möglich.
     * </p>
     *
     * @param email    Die E-Mail Adresse des Kunden
     * @param password Das Passwort des Kunden
     * @return Den eingeloggten Kunden oder {@code null} falls der Kunde nicht eingeloggt werden konnte
     */
    @Nullable
    @Override
    public Customer login(String email, String password) {
        return null;
    }

    /**
     * Aktualisiert die Profilinformationen eines Kunden.
     * <p>
     * <p>
     * <b>Hinweis</b>: Es sollen die Profilinformationen des aktuell eingeloggten Kunden aktualisiert werden.
     * </p>
     *
     * @param update Die aktualisierten Profilinformationen
     */
    @Override
    public void updateProfile(Customer update) {

    }

    /**
     * Erstellt ein Newsletter-Abonnement für einen Kunden, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Ein Kunde darf Newsletter nicht für andere Kunden abonnieren.
     * </p>
     *
     * @param newsletter Der Newsletter, welcher abonniert wird
     */
    @Override
    public void subscribe(Newsletter newsletter) {

    }

    /**
     * Abonniert ein Lieferabo, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Ein Kunde darf Lieferabos nicht für andere Kunden abonnieren.
     * </p>
     *
     * @param subscription Das Lieferabo, welches abonniert wird
     */
    @Override
    public void subscribe(Subscription subscription) {

    }

    /**
     * Bestellt ein bestehendes Lieferabo ab, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Ein Kunde darf Lieferabos nicht für andere Kunden abbestellen.
     * </p>
     *
     * @param subscription Das Lieferabo, welches abbestellt wird
     */
    @Override
    public void unsubscribe(Subscription subscription) {

    }

    /**
     * Gibt einem gewöhnlichen Kunden eine Premiummitgliedschaft, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param customer Der Kunde, welcher eine Premiummitgliedschaft erhalten soll
     * @param endDate  Das Datum, an dem die Premiummitgliedschaft endet
     * @param image    Der Studentenausweis, falls vorhanden
     */
    @Override
    public void makePremium(Customer customer, LocalDateTime endDate, ImageIcon image) {

    }

    /**
     * Erstellt einen neuen kundengebundenen Warenkorb.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von dem aktuell eingeloggten Kunden genutzt werden.
     * </p>
     *
     * @param customer Der Kunde, welcher einen neuen Warenkorb anlegen möchte
     */
    @Override
    public void createCart(Customer customer) {

    }

    /**
     * Entfernt einen neuen kundengebundenen Warenkorb.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur vom aktuell eingeloggten Kunden genutzt werden.
     * </p>
     *
     * @param cart Der zu entfernende Warenkorb
     */
    @Override
    public void removeCart(Cart cart) {

    }

    /**
     * Ermittelt alle kundengebundenen Warenkörbe.
     * <p>
     * <p>
     * <b>Hinweis</b>: Der aktuell eingeloggte Kunde darf nur seine eigenen Warenkörbe sehen.
     * </p>
     *
     * @return Eine Liste aller zum eingeloggten Kunden gehörenden Warenkörbe oder eine leere List, falls der Kunde
     * keine Warenkörbe besitzt.
     */
    @Override
    public List<Cart> getCarts() {
        return null;
    }

    /**
     * Legt ein Angebot in der angegebenen Anzahl in den Warenkorb des Kunden, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von dem Kunden, welchem der
     * Warenkorb gehört, genutzt werden.
     * </p>
     *
     * @param cart  Der Warenkorb, welcher dem Kunden gehört
     * @param entry Der Eintrag, welcher dem Warenkorb hinzugefügt werden soll
     */
    @Override
    public void putOffer(Cart cart, Cart.Entry entry) {

    }

    /**
     * Entfernt ein Angebot aus dem Warenkorb des Kunden, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von dem Kunden, welchem der
     * Warenkorb gehört, genutzt werden.
     * </p>
     *
     * @param cart  Der Warenkorb, welcher dem Kunden gehört
     * @param offer Das Angebot, welches aus dem Warenkorb entfernt werden soll
     */
    @Override
    public void removeOffer(Cart cart, Offer offer) {

    }

    /**
     * Bestellt den Warenkorb eines Kunden, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Kunden dürfen Warenkörbe nicht für andere Kunden bestellen.
     * </p>
     *
     * @param cart Der Warenkorb, welcher dem Kunden gehört
     */
    @Override
    public void orderCart(Cart cart) {

    }

    /**
     * Erstellt einen neuen Tag, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param tag Der zu erstellende Tag
     */
    @Override
    public void createTag(Tag tag) {

    }

    /**
     * Entfernt einen bestehenden Tag, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden. Ein Schlagwort darf nur dann gelöscht
     * werden, wenn es nicht mehr in Benutzung ist.
     * </p>
     *
     * @param tag Der zu entfernende Tag
     */
    @Override
    public void deleteTag(Tag tag) {

    }

    /**
     * Erstellt einen neuen Artikel, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param article Der zu erstellende Artikel
     */
    @Override
    public void createArticle(Article article) {

    }

    /**
     * Entfernt einen bestehenden Artikel, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param article Der zu entfernende Artikel
     */
    @Override
    public void deleteArticle(Article article) {

    }

    /**
     * Erstellt ein neues Angebot, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param offer  Das zu erstellende Angebot
     * @param vendor Der Anbieter
     * @param count  Der Bestand
     */
    @Override
    public void createOffer(Offer offer, Vendor vendor, int count) {

    }

    /**
     * Entfernt ein bestehendes Angebot, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param offer Das zu entfernende Angebot
     */
    @Override
    public void deleteOffer(Offer offer) {

    }

    /**
     * Aktualisiert einen bestehenden Artikel, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param article Der zu aktualisierende Artikel
     */
    @Override
    public void updateArticle(Article article) {

    }

    /**
     * Erstellt einen neuen Newsletter, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param newsletter Der zu erstellende Newsletter
     */
    @Override
    public void createNewsletter(Newsletter newsletter) {

    }

    /**
     * Sucht nach allen bestellten Warenkörben.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @return Die Liste aller bestellten Warenkörbe oder eine leere Liste,
     * falls keine bestellten Warenkörbe existieren.
     */
    @Override
    public List<Cart> getOrderedCarts() {
        return null;
    }

    /**
     * Fügt einem Artikel eine Empfehlung hinzu, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param recommender Der empfehlende Artikel
     * @param recommended Der empfohlene Artikel
     */
    @Override
    public void addRecommendation(Article recommender, Article recommended) {

    }

    /**
     * Entfernt eine Empfehlung eines Artikels, falls möglich.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf nur von Angestellten genutzt werden.
     * </p>
     *
     * @param recommender Der empfehlende Artikel
     * @param recommended Der empfohlene Artikel
     */
    @Override
    public void removeRecommendation(Article recommender, Article recommended) {

    }

    /**
     * Sucht nach Artikeln anhand ihrer Namen.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf von allen Kunden genutzt werden.
     * </p>
     *
     * @param name Der Namen der zu suchenden Artikel
     * @return Eine Liste aller gefundenen Artikel oder eine leere Liste,
     * falls keine Artikel mit entsprechendem Namen existieren.
     */
    @Override
    public List<Article> searchByName(String name) {
        return null;
    }

    /**
     * Ermittelt alle Angebote und sortiert sie entsprechend der angegebenen Sortierung.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf von allen Kunden genutzt werden.
     * </p>
     *
     * @param sortingOrder Sortierung
     * @return Eine sortierte Liste aller Angebote oder eine leere Liste, falls keine Angebote existieren.
     */
    @Override
    public List<Offer> getSortedOffers(SortingOrder sortingOrder) {
        return null;
    }

    /**
     * Ermittelt alle Artikel die einen oder mehrere der angegebenen Tags beinhalten.
     * <p>
     * <p>
     * <b>Hinweis</b>: Diese Methode darf von allen Kunden genutzt werden.
     * </p>
     *
     * @param tags Die zu suchenden Tags
     * @return Eine Liste aller gefundenen Tags oder eine leere Liste, falls keine Artikel gefunden wurden.
     */
    @Override
    public List<Article> getArticlesByTag(Tag... tags) {
        return null;
    }

    /**
     * Löscht alle Einträge innerhalb der Datenbank.
     * <p>
     * <p>
     * <b>Hinweis</b>: Es soll lediglich der Inhalt aller Tabellen entfernt werden.
     * </p>
     */
    @Override
    public void deleteAll() {

    }

    @Override
    public String getConnectionUrl() {
        return CONNECTION_URL;
    }

}
