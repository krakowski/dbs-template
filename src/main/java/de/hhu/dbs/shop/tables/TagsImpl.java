package de.hhu.dbs.shop.tables;

import de.hhu.dbs.warehouse.db.table.Tags;
import de.hhu.dbs.warehouse.pojo.Tag;

import java.sql.SQLException;
import java.util.List;

public class TagsImpl extends Tags {

    /**
     * Fragt ein Schlagwort anhand seines eindeutigen Namen ab.
     *
     * @param name Der Name des zu suchenden Schlagworts
     * @return Das gefundene Schlagwort oder {@code null} falls keines gefunden wurde
     */
    @Override
    public Tag getByName(String name) {
        return null;
    }

    /**
     * Fragt alle Zeilen innerhalb dieser Tabelle ab.
     *
     * @return Eine Liste aller Einträge dieser Tabelle oder ein leere Liste, falls die
     * Tabelle keine Einträge enthält.
     */
    @Override
    public List<Tag> getAll() {
        return null;
    }

    /**
     * Fragt eine neue Zeile innerhalb dieser Tabelle ein.
     *
     * @param object Das einzufügende Objekt
     */
    @Override
    public void insert(Tag object) {

    }

    /**
     * Löscht eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu löschende Objekt
     */
    @Override
    public void delete(Tag object) {

    }

    /**
     * Aktualisiert eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu aktualisierende Objekt
     */
    @Override
    public void update(Tag object) {

    }

    /**
     * Entfernt alle Einträge dieser Tabelle
     */
    @Override
    public void deleteAll() {

    }

    /**
     * Zählt die Einträge innerhalb dieser Tabelle
     *
     * @return Die Anzahl der vorhandenen Einträge
     */
    @Override
    public int count() {
        return 0;
    }
}
