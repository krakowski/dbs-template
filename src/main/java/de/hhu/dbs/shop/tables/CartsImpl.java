package de.hhu.dbs.shop.tables;

import de.hhu.dbs.warehouse.db.table.Carts;
import de.hhu.dbs.warehouse.pojo.Cart;

import java.sql.SQLException;
import java.util.List;

public class CartsImpl extends Carts {

    /**
     * Fragt einen Warenkorb anhand seiner eindeutigen Id ab.
     *
     * @param id Die Id des zu suchenden Warenkorbs
     * @return Den gefundenen Warenkorb oder {@code null} falls keiner gefunden wurde
     */
    @Override
    public Cart getById(long id) {
        return null;
    }

    /**
     * Fragt alle Zeilen innerhalb dieser Tabelle ab.
     *
     * @return Eine Liste aller Einträge dieser Tabelle oder ein leere Liste, falls die
     * Tabelle keine Einträge enthält.
     */
    @Override
    public List<Cart> getAll() {
        return null;
    }

    /**
     * Fragt eine neue Zeile innerhalb dieser Tabelle ein.
     *
     * @param object Das einzufügende Objekt
     */
    @Override
    public void insert(Cart object) {

    }

    /**
     * Löscht eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu löschende Objekt
     */
    @Override
    public void delete(Cart object) {

    }

    /**
     * Aktualisiert eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu aktualisierende Objekt
     */
    @Override
    public void update(Cart object) {

    }

    /**
     * Entfernt alle Einträge dieser Tabelle
     */
    @Override
    public void deleteAll() {

    }

    /**
     * Zählt die Einträge innerhalb dieser Tabelle
     *
     * @return Die Anzahl der vorhandenen Einträge
     */
    @Override
    public int count() {
        return 0;
    }
}
