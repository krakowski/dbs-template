package de.hhu.dbs.shop.tables;

import de.hhu.dbs.warehouse.db.table.Couriers;
import de.hhu.dbs.warehouse.pojo.Courier;

import javax.annotation.Nullable;
import java.util.List;

public class CouriersImpl extends Couriers {

    /**
     * Fragt einen Lieferdienst anhand seiner eindeutigen Bezeichnung ab.
     *
     * @param name Die Bezeichnung des zu suchenden Lieferdienstes
     * @return Den gefundenen Lieferdienst oder {@code null} falls keiner gefunden wurde
     */
    @Override
    @Nullable
    public Courier getByName(String name) {
        return null;
    }

    /**
     * Fragt alle Zeilen innerhalb dieser Tabelle ab.
     *
     * @return Eine Liste aller Einträge dieser Tabelle oder ein leere Liste, falls die
     * Tabelle keine Einträge enthält.
     */
    @Override
    public List<Courier> getAll() {
        return null;
    }

    /**
     * Fragt eine neue Zeile innerhalb dieser Tabelle ein.
     *
     * @param object Das einzufügende Objekt
     */
    @Override
    public void insert(Courier object) {

    }

    /**
     * Löscht eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu löschende Objekt
     */
    @Override
    public void delete(Courier object) {

    }

    /**
     * Aktualisiert eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu aktualisierende Objekt
     */
    @Override
    public void update(Courier object) {

    }

    /**
     * Entfernt alle Einträge dieser Tabelle
     */
    @Override
    public void deleteAll() {

    }

    /**
     * Zählt die Einträge innerhalb dieser Tabelle
     *
     * @return Die Anzahl der vorhandenen Einträge
     */
    @Override
    public int count() {
        return 0;
    }
}
