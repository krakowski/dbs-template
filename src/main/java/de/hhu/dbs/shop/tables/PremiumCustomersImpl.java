package de.hhu.dbs.shop.tables;

import de.hhu.dbs.warehouse.db.table.PremiumCustomers;
import de.hhu.dbs.warehouse.pojo.PremiumCustomer;

import java.util.List;

public class PremiumCustomersImpl extends PremiumCustomers {

    /**
     * Fragt einen Premiumkunden anhand seiner eindeutigen E-Mail Adresse ab.
     *
     * @param email Die E-Mail Adresse des zu suchenden Premiumkunden
     * @return Den gefundenen Premiumkunden oder {@code null} falls keiner gefunden wurde
     */
    @Override
    public PremiumCustomer getByEmail(String email) {
        return null;
    }

    /**
     * Fragt alle Zeilen innerhalb dieser Tabelle ab.
     *
     * @return Eine Liste aller Einträge dieser Tabelle oder ein leere Liste, falls die
     * Tabelle keine Einträge enthält.
     */
    @Override
    public List<PremiumCustomer> getAll() {
        return null;
    }

    /**
     * Fragt eine neue Zeile innerhalb dieser Tabelle ein.
     *
     * @param object Das einzufügende Objekt
     */
    @Override
    public void insert(PremiumCustomer object) {

    }

    /**
     * Löscht eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu löschende Objekt
     */
    @Override
    public void delete(PremiumCustomer object) {

    }

    /**
     * Aktualisiert eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu aktualisierende Objekt
     */
    @Override
    public void update(PremiumCustomer object) {

    }

    /**
     * Entfernt alle Einträge dieser Tabelle
     */
    @Override
    public void deleteAll() {

    }

    /**
     * Zählt die Einträge innerhalb dieser Tabelle
     *
     * @return Die Anzahl der vorhandenen Einträge
     */
    @Override
    public int count() {
        return 0;
    }
}
