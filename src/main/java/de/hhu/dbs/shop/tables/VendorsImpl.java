package de.hhu.dbs.shop.tables;

import de.hhu.dbs.warehouse.db.table.Vendors;
import de.hhu.dbs.warehouse.pojo.Vendor;

import java.util.List;

public class VendorsImpl extends Vendors {

    /**
     * Fragt einen Anbieter anhand seiner eindeutigen Bezeichnung ab.
     *
     * @param name Die Bezeichnung des zu suchenden Anbieters
     * @return Den gefundenen Anbieter oder {@code null} falls keiner gefunden wurde
     */
    @Override
    public Vendor getByName(String name) {
        return null;
    }

    /**
     * Fragt alle Zeilen innerhalb dieser Tabelle ab.
     *
     * @return Eine Liste aller Einträge dieser Tabelle oder ein leere Liste, falls die
     * Tabelle keine Einträge enthält.
     */
    @Override
    public List<Vendor> getAll() {
        return null;
    }

    /**
     * Fragt eine neue Zeile innerhalb dieser Tabelle ein.
     *
     * @param object Das einzufügende Objekt
     */
    @Override
    public void insert(Vendor object) {

    }

    /**
     * Löscht eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu löschende Objekt
     */
    @Override
    public void delete(Vendor object) {

    }

    /**
     * Aktualisiert eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu aktualisierende Objekt
     */
    @Override
    public void update(Vendor object) {

    }

    /**
     * Entfernt alle Einträge dieser Tabelle
     */
    @Override
    public void deleteAll() {

    }

    /**
     * Zählt die Einträge innerhalb dieser Tabelle
     *
     * @return Die Anzahl der vorhandenen Einträge
     */
    @Override
    public int count() {
        return 0;
    }
}
