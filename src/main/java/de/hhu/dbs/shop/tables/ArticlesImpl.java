package de.hhu.dbs.shop.tables;

import de.hhu.dbs.warehouse.db.table.Articles;
import de.hhu.dbs.warehouse.pojo.Article;

import javax.annotation.Nullable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class ArticlesImpl extends Articles {

    /**
     * Fragt einen Artikel anhand seiner eindeutigen Id ab.
     *
     * @param id Die Id des zu suchenden Artikels
     * @return Den gefundenen Artikel oder {@code null} falls keiner gefunden wurde
     */
    @Override
    @Nullable
    public Article getById(long id) {
        return null;
    }

    /**
     * Fragt alle Zeilen innerhalb dieser Tabelle ab.
     *
     * @return Eine Liste aller Einträge dieser Tabelle oder ein leere Liste, falls die
     * Tabelle keine Einträge enthält.
     */
    @Override
    public List<Article> getAll() {
        return null;
    }

    /**
     * Fragt eine neue Zeile innerhalb dieser Tabelle ein.
     *
     * @param object Das einzufügende Objekt
     */
    @Override
    public void insert(Article object) {

    }

    /**
     * Löscht eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu löschende Objekt
     */
    @Override
    public void delete(Article object) {

    }

    /**
     * Aktualisiert eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu aktualisierende Objekt
     */
    @Override
    public void update(Article object) {

    }

    /**
     * Entfernt alle Einträge dieser Tabelle
     */
    @Override
    public void deleteAll() {

    }

    /**
     * Zählt die Einträge innerhalb dieser Tabelle
     *
     * @return Die Anzahl der vorhandenen Einträge
     */
    @Override
    public int count() {
        return 0;
    }


}
