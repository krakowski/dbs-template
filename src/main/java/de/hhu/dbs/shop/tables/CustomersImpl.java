package de.hhu.dbs.shop.tables;

import de.hhu.dbs.shop.Shop;
import de.hhu.dbs.warehouse.db.table.Customers;
import de.hhu.dbs.warehouse.pojo.Customer;

import java.util.List;
import java.util.Optional;

public class CustomersImpl extends Customers {

    /**
     * Fragt einen Kunden anhand seiner eindeutigen E-Mail Adresse ab.
     *
     * @param email Die E-Mail Adresse des zu suchenden Artikels
     * @return Den gefundenen Kunden oder {@code null} falls keiner gefunden wurde
     */
    @Override
    public Customer getByEmail(String email) {
        return null;
    }

    /**
     * Fragt alle Zeilen innerhalb dieser Tabelle ab.
     *
     * @return Eine Liste aller Einträge dieser Tabelle oder ein leere Liste, falls die
     * Tabelle keine Einträge enthält.
     */
    @Override
    public List<Customer> getAll() {
        return null;
    }

    /**
     * Fragt eine neue Zeile innerhalb dieser Tabelle ein.
     *
     * @param object Das einzufügende Objekt
     */
    @Override
    public void insert(Customer object) {

    }

    /**
     * Löscht eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu löschende Objekt
     */
    @Override
    public void delete(Customer object) {

    }

    /**
     * Aktualisiert eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu aktualisierende Objekt
     */
    @Override
    public void update(Customer object) {

    }

    /**
     * Entfernt alle Einträge dieser Tabelle
     */
    @Override
    public void deleteAll() {

    }

    /**
     * Zählt die Einträge innerhalb dieser Tabelle
     *
     * @return Die Anzahl der vorhandenen Einträge
     */
    @Override
    public int count() {
        return 0;
    }

}
