package de.hhu.dbs.shop.tables;

import de.hhu.dbs.warehouse.db.table.Subscriptions;
import de.hhu.dbs.warehouse.pojo.Subscription;

import java.util.List;

public class SubscriptionsImpl extends Subscriptions {

    /**
     * Fragt einen Warenkorb anhand seiner eindeutigen id ab.
     *
     * @param id Die Id des zu suchenden Warenkorbs
     * @return Den gefundenen Warenkorb oder {@code null} falls keiner gefunden wurde
     */
    @Override
    public Subscription getById(long id) {
        return null;
    }

    /**
     * Fragt alle Zeilen innerhalb dieser Tabelle ab.
     *
     * @return Eine Liste aller Einträge dieser Tabelle oder ein leere Liste, falls die
     * Tabelle keine Einträge enthält.
     */
    @Override
    public List<Subscription> getAll() {
        return null;
    }

    /**
     * Fragt eine neue Zeile innerhalb dieser Tabelle ein.
     *
     * @param object Das einzufügende Objekt
     */
    @Override
    public void insert(Subscription object) {

    }

    /**
     * Löscht eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu löschende Objekt
     */
    @Override
    public void delete(Subscription object) {

    }

    /**
     * Aktualisiert eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu aktualisierende Objekt
     */
    @Override
    public void update(Subscription object) {

    }

    /**
     * Entfernt alle Einträge dieser Tabelle
     */
    @Override
    public void deleteAll() {

    }

    /**
     * Zählt die Einträge innerhalb dieser Tabelle
     *
     * @return Die Anzahl der vorhandenen Einträge
     */
    @Override
    public int count() {
        return 0;
    }
}
