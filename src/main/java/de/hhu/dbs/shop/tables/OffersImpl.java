package de.hhu.dbs.shop.tables;

import de.hhu.dbs.warehouse.db.table.Offers;
import de.hhu.dbs.warehouse.pojo.Offer;

import java.util.List;

public class OffersImpl extends Offers {

    /**
     * Fragt ein Angebot anhand seiner eindeutigen Id ab.
     *
     * @param id Die Id des zu suchenden Angebots
     * @return Das gefundene Angebot oder {@code null} falls keines gefunden wurde
     */
    @Override
    public Offer getById(long id) {
        return null;
    }

    /**
     * Fragt alle Zeilen innerhalb dieser Tabelle ab.
     *
     * @return Eine Liste aller Einträge dieser Tabelle oder ein leere Liste, falls die
     * Tabelle keine Einträge enthält.
     */
    @Override
    public List<Offer> getAll() {
        return null;
    }

    /**
     * Fragt eine neue Zeile innerhalb dieser Tabelle ein.
     *
     * @param object Das einzufügende Objekt
     */
    @Override
    public void insert(Offer object) {

    }

    /**
     * Löscht eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu löschende Objekt
     */
    @Override
    public void delete(Offer object) {

    }

    /**
     * Aktualisiert eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu aktualisierende Objekt
     */
    @Override
    public void update(Offer object) {

    }

    /**
     * Entfernt alle Einträge dieser Tabelle
     */
    @Override
    public void deleteAll() {

    }

    /**
     * Zählt die Einträge innerhalb dieser Tabelle
     *
     * @return Die Anzahl der vorhandenen Einträge
     */
    @Override
    public int count() {
        return 0;
    }
}
