package de.hhu.dbs.shop.tables;

import de.hhu.dbs.warehouse.db.table.Employees;
import de.hhu.dbs.warehouse.pojo.Employee;

import java.util.List;

public class EmployeesImpl extends Employees {

    /**
     * Fragt einen Angestellten anhand seiner eindeutigen E-Mail Adresse ab.
     *
     * @param email Die E-Mail Adresse des zu suchenden Angestellten
     * @return Den gefundenen Angestellten oder {@code null} falls keiner gefunden wurde
     */
    @Override
    public Employee getByEmail(String email) {
        return null;
    }

    /**
     * Fragt alle Zeilen innerhalb dieser Tabelle ab.
     *
     * @return Eine Liste aller Einträge dieser Tabelle oder ein leere Liste, falls die
     * Tabelle keine Einträge enthält.
     */
    @Override
    public List<Employee> getAll() {
        return null;
    }

    /**
     * Fragt eine neue Zeile innerhalb dieser Tabelle ein.
     *
     * @param object Das einzufügende Objekt
     */
    @Override
    public void insert(Employee object) {

    }

    /**
     * Löscht eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu löschende Objekt
     */
    @Override
    public void delete(Employee object) {

    }

    /**
     * Aktualisiert eine Zeile innerhalb dieser Tabelle
     *
     * @param object Das zu aktualisierende Objekt
     */
    @Override
    public void update(Employee object) {

    }

    /**
     * Entfernt alle Einträge dieser Tabelle
     */
    @Override
    public void deleteAll() {

    }

    /**
     * Zählt die Einträge innerhalb dieser Tabelle
     *
     * @return Die Anzahl der vorhandenen Einträge
     */
    @Override
    public int count() {
        return 0;
    }
}
